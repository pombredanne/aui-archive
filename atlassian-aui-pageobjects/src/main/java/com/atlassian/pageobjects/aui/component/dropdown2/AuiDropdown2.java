package com.atlassian.pageobjects.aui.component.dropdown2;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.aui.component.ElementBounding;
import com.atlassian.pageobjects.binder.Init;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementActions;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.WebDriverElement;
import com.atlassian.webdriver.AtlassianWebDriver;
import com.atlassian.webdriver.utils.by.ByJquery;
import com.atlassian.webdriver.waiter.Waiter;
import com.atlassian.webdriver.waiter.webdriver.function.ConditionFunction;
import com.google.common.base.Preconditions;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import javax.inject.Inject;
import java.util.concurrent.TimeUnit;

public class AuiDropdown2 {

    @Inject
    PageElementFinder elementFinder;
    
    @Inject
    Waiter waiter;
    
    @Inject
    AtlassianWebDriver driver;
    
    @Inject
    PageBinder binder;

    @Inject
    private PageElementActions actions;

    private WebDriverElement dropdownTrigger;
    private WebDriverElement dropdown;

    private By triggerLocator = null;
    
    private ConditionFunction dropdownOpenCondition = new ConditionFunction() {
        public Boolean apply(WebDriver webDriver) {
            return dropdown.isPresent() && dropdown.isVisible();
        }
    };
    
    public AuiDropdown2(By triggerLocator) {
        this.triggerLocator = Preconditions.checkNotNull(triggerLocator,
                "The by locator for the dropdown trigger cannot be null.");
    }
    
    public AuiDropdown2(PageElement dropdownTrigger)
    {
        this.dropdownTrigger = (WebDriverElement) dropdownTrigger;
    }
    
    @Init
    public void init() {
        if (triggerLocator != null)
        {
            dropdownTrigger = (WebDriverElement) elementFinder.find(triggerLocator);
        }
        dropdown = (WebDriverElement) elementFinder.find(getDropdownLocator());
    }

    public AuiDropdown2 open() {
        return open(true);
    }

    public AuiDropdown2 open(boolean shouldWait)
    {
        if (!isOpen()) {
            dropdownTrigger.click();
        }

        if (shouldWait)
        {
            waitForDropdownToOpen();
        }

        return this;
    }
    
    public AuiDropdown2 close() {
        
        if(isOpen()) {
            Actions action = new Actions(driver);

            WebElement el = driver.findElement(ByJquery.$("#" + dropdown.getAttribute("id")).parent());
            action.click(el).perform();
        }

        waitForDropdownToClose();
        
        return this;
    }
    
    public AuiDropdown2 closeWithEscape() {
        
        if (isOpen()) {
            new Actions(driver).sendKeys(Keys.ESCAPE).perform();
        }

        waitForDropdownToClose();
        
        return this;
    }

    public boolean isOpen() {
        return dropdown.isVisible();
    }
    
    public boolean isDisabled()
    {
        String ariaDisabled = dropdownTrigger.getAttribute("aria-disabled");
        return "true".equals(ariaDisabled);
    }
    
    public int getTriggerWidth () {
        return dropdownTrigger.asWebElement().getSize().getWidth();
    }

    public int getDropdownWidth() {
        assertDropdownIsOpen("The dropdown must be open in order to check the width.");

        return dropdown.asWebElement().getSize().getWidth();
    }

    public AuiDropdown2Items getDropdownItems()
    {
        assertDropdownIsOpen("The dropdown must be open in order to get the dropdown list items.");
        
        return binder.bind(AuiDropdown2Items.class, getDropdownLocator());
    }

    public ElementBounding getDropdownBounding()
    {
        assertDropdownIsOpen("The dropdown must be open in oder to get the dropdown position.");
        
        return new ElementBounding(dropdown);
    }
    
    public boolean isInDropdownGroup() {
        return getDropdownGroup().isPresent();
    }

    public String getAlignment(){
        return dropdown.asWebElement().getAttribute("data-dropdown2-alignment");
    }
    
    public AuiDropdown2 getNextDropdownInGroup() {
        //todo: assert that its open.
        //todo: assert that there is a next.
        actions.sendKeys(Keys.ARROW_RIGHT).perform();
        
        PageElement nextDropdownTrigger = getDropdownGroup().find(By.cssSelector("a.aui-dropdown2-trigger.active"));
        
        return binder.bind(AuiDropdown2.class, nextDropdownTrigger);
    }
    
    private PageElement getDropdownGroup()
    {
        // bug: passing page object ot ByJquery should work but doesn't. Workaround: can hard code ID if required.
        PageElement group = elementFinder.find(ByJquery.$(dropdownTrigger.asWebElement()).parents(".aui-dropdown2-trigger-group"));
        return group;
    }
    
    private By getDropdownLocator() {
        String ariaOwns = dropdownTrigger.getAttribute("aria-owns");
        Preconditions.checkState(ariaOwns != null, "The aria-owns attribute must be defined on the dropdown2 trigger");
        return By.id(ariaOwns);
    }

    private void assertDropdownIsOpen(String message)
    {
        Preconditions.checkState(isOpen(), message);
    }
    
    private void waitForDropdownToOpen() {
        waiter.until(5, TimeUnit.SECONDS).function(dropdownOpenCondition).isTrue().execute();
    }

    private void waitForDropdownToClose() {
        waiter.until(5, TimeUnit.SECONDS).function(dropdownOpenCondition).isFalse().execute();
    }

}
