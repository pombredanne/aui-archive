#!/bin/bash
usage="\n USAGE:
\n ./aui.sh install \t - clean install AUI. Required at least once for fresh checkouts.
\n ./aui.sh flatpack \t - generate a local snapshot version of the flat pack.
\n ./aui.sh qunit \t - start the QUnit test harness.
\n ./aui.sh refapp \t - start the AUI REFAPP.
\n ./aui.sh debug \t - start the AUI REFAPP in DEBUG mode (so that you can attach a remote debugger).
\n ./aui.sh test \t\t - run the AUI unit and integration tests (including experimental tests).
\n ./aui.sh testcoreonly \t - run the core tests only (exclude experimental).
\n"

if [[ $1 ]]
then

    # We need to be sure we're in the right place, so go to the script dir.
    SOURCE="${BASH_SOURCE[0]}"
    DIR="$( dirname "$SOURCE" )"
    cd $DIR

    if [ $1 == "refapp" -o $1 == "debug" ]
    then
        # refapp MUST be run from subdir
        cd auiplugin-tests
        if [ $1 == "debug" ]
        then
            echo "[INFO] Running the AUI refapp. Skipping tests and disabling caches."
            atlas-mvn amps:debug -DskipTests -DskipAllPrompts=true -Datlassian.disable.caches=true -Dplugin.resource.directories=../auiplugin/src/main/resources
        else
            echo "[INFO] Running the AUI refapp. Skipping tests and disabling caches."
            atlas-mvn amps:run -DskipTests -DskipAllPrompts=true -Datlassian.disable.caches=true -Dplugin.resource.directories=../auiplugin/src/main/resources
        fi

    elif [ $1 == "flatpack" ]
    then
        echo "[INFO] Building AUI flat pack."
        atlas-mvn -pl aui-flat-pack package -DskipAllPrompts=true
        echo "[DONE] See /aui-flat-pack/target/flatpack/ for results of build."
        echo "[DONE] open ./aui-flat-pack/target/flatpack/aui-examples.html to view examples."

    elif [ $1 == "install" ]
    then
        echo "[INFO] Clean-installing Atlassian REFAPP. This could take a while."
        atlas-mvn clean install -DskipTests -DskipAllPrompts=true

    elif [ $1 == "test" ]
    then
        echo "[INFO] Running full AUI test suite (including experimental tests)"
        echo "[NOTE] If you are running a firewall, you may need to grant permission to the test browser."
        atlas-mvn -Pexperimental verify -DskipAllPrompts=true

    elif [ $1 == "testcoreonly" ]
    then
        echo "[INFO] Running core AUI Unit and Integration tests (excludes experimental tests)"
        echo "[NOTE] If you are running a firewall, you may need to grant permission to the test browser."
        atlas-mvn clean integration-test -DskipAllPrompts=true

    elif [ $1 == "qunit" ]
    then
        echo "[INFO] Running QUnit test harness. NOTE: Mac only and requires 'mvn' instead of 'atlas-mvn'"
        cd auiplugin
        mvn -Pexperimental com.atlassian.maven.plugins:qunit-maven-plugin:run-server -DskipAllPrompts=true

    else
        echo -e $usage
    fi

else 
    echo -e $usage
fi