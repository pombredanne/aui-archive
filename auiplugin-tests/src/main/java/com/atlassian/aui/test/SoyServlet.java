package com.atlassian.aui.test;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;
import java.util.Map;

public class SoyServlet extends HttpServlet
{
    // ------------------------------------------------------------------------------------------------------- Constants
    private static final String CONTENT_TYPE = "text/html;charset=UTF-8";
    public static final String SERVER_SOY_MODULE_KEY = "auiplugin-tests:soy-test-pages";
    // ------------------------------------------------------------------------------------------------- Type Properties
    // ---------------------------------------------------------------------------------------------------- Dependencies
    private final WebResourceManager webResourceManager;
    private final Plugin plugin;
    private final SoyTemplateRenderer soyTemplateRenderer;
    // ---------------------------------------------------------------------------------------------------- Constructors

    public SoyServlet(WebResourceManager webResourceManager, PluginAccessor pluginAccessor, SoyTemplateRenderer soyTemplateRenderer)
    {
        this.webResourceManager = webResourceManager;
        this.plugin = pluginAccessor.getPlugin("auiplugin-tests");
        this.soyTemplateRenderer = soyTemplateRenderer;
    }

    // ----------------------------------------------------------------------------------------------- Interface Methods
    // -------------------------------------------------------------------------------------------------- Action Methods
    // -------------------------------------------------------------------------------------------------- Public Methods
    // ------------------------------------------------------------------------------------------------- Helper Methods

    /**
     * Constructs a Soy namespace from the path
     * @param pathInfo {@link String} representing a path, e.g. /test-pages/experimental/page-layout/index.soy
     * @return {@link String} representing a camelCased Soy namespace, e.g. testPages.experimental.pageLayout.index
     */
    protected static String pathToSoyTemplate(String pathInfo)
    {
        StringBuilder stringBuilder = new StringBuilder();
        boolean lastCharWasDash = false;
        final char[] chars = pathInfo.toCharArray();

        for (int i = 0 ; i < chars.length ; i++)
        {
            char c = chars[i];
            if (c == '-')
            {
                lastCharWasDash = true;
            }
            else
            {
                if (c == '/')
                {
                    // we don't want the slash if its the first character in the path
                    if (i != 0)
                    {
                        stringBuilder.append(".");
                    }
                }
                else if (lastCharWasDash)
                {
                    stringBuilder.append(Character.toUpperCase(c));
                }
                else
                {
                    stringBuilder.append(c);
                }
                lastCharWasDash = false;
            }
        }

        String newString = stringBuilder.toString();
        if (newString.endsWith(".soy"))
        {
            newString = newString.substring(0, newString.length()-4);
        }
        return newString;
    }

    /**
     * Remove the context path from the path
     * @param pathInfo {@link String} the path
     * @return {@link String} the path with the context path removed
     */
    protected static String stripContextPath(String pathInfo)
    {
        return pathInfo.replaceFirst("/ajstest", "");
    }
    // -------------------------------------------------------------------------------------- Basic Accessors / Mutators

    /**
     * Looks at the path info, and either serves a directory listing, or a rendered soy file
     * @param req {@link HttpServletRequest}
     * @param resp {@link HttpServletResponse}
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        Map<String, Object> context = Maps.newHashMap();
        context.put("webResourceManager", webResourceManager);
        resp.setContentType(CONTENT_TYPE);

        final String pathInfo = stripContextPath(req.getPathInfo());
        if (pathInfo.endsWith("/"))
        {
            try
            {
                displayIndex(pathInfo, resp, context);
            }
            catch (URISyntaxException e)
            {
                throw new IOException(e);
            }
        }
        else
        {
            try
            {
                soyTemplateRenderer.render(resp.getWriter(), SERVER_SOY_MODULE_KEY, pathToSoyTemplate(pathInfo), context);
            }
            catch (SoyException e)
            {
                if (e.getCause() instanceof IOException)
                {
                    throw (IOException) e.getCause();
                }
                else
                {
                    throw new ServletException(e);
                }
            }
        }

        resp.getWriter().close();
    }

    /**
     * Tries to find and serve a rendered soy file called "index.soy", or a directory listing
     * @param pathInfo {@link String} the path to operate on
     * @param resp {@link HttpServletResponse}
     * @param context {@link Map}
     * @throws ServletException
     * @throws IOException
     * @throws URISyntaxException
     */
    protected void displayIndex(String pathInfo, HttpServletResponse resp, Map<String, Object> context) throws ServletException, IOException, URISyntaxException
    {
        URL fileURL = plugin.getResource(pathInfo);
        if (fileURL == null)
        {
            resp.sendError(404);
            return;
        }
        else if ("file".equals(fileURL.getProtocol().toLowerCase()))
        {
            final String indexPath = pathInfo + "index.soy";
            URL indexURL = plugin.getResource(indexPath);
            if (indexURL != null)
            {
                try
                {
                    soyTemplateRenderer.render(resp.getWriter(), SERVER_SOY_MODULE_KEY, pathToSoyTemplate(indexPath), context);
                }
                catch (SoyException e)
                {
                    if (e.getCause() instanceof IOException)
                    {
                        throw (IOException) e.getCause();
                    }
                    else
                    {
                        throw new ServletException(e);
                    }
                }
            }
            else
            {
                File file = new File(fileURL.toURI());
                List<String> files = Lists.newArrayList("..");
                final File[] fileList = file.listFiles();
                if (fileList != null)
                {
                    for (File kid : fileList)
                    {
                        String name = kid.getName();
                        if (kid.isDirectory())
                        {
                            name += "/";
                        }
                        files.add(name);
                    }
                }
                context.put("files", files);
                try
                {
                    soyTemplateRenderer.render(resp.getWriter(), SERVER_SOY_MODULE_KEY, "testPages.filelist", context);
                }
                catch (SoyException e)
                {
                    if (e.getCause() instanceof IOException)
                    {
                        throw (IOException) e.getCause();
                    }
                    else
                    {
                        throw new ServletException(e);
                    }
                }
            }
        }
    }
}