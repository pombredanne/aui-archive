package it.com.atlassian.aui.javascript.integrationTests;

import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.WebDriverElement;
import it.com.atlassian.aui.javascript.AbstractAuiIntegrationTest;
import it.com.atlassian.aui.javascript.pages.DialogTestPage;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class AUIDialogTest extends AbstractAuiIntegrationTest
{
    private DialogTestPage dialogTestPage;
    private PageElementFinder elementFinder;

    @Before
    public void setup()
    {
        dialogTestPage = product.visit(DialogTestPage.class);
        elementFinder = dialogTestPage.getElementFinder();
    }

    @Test
    public void testPopupDim()
    {
        elementFinder.find(By.id("popup-button")).click();

        PageElement blanket = elementFinder.find(By.className("aui-blanket"));

        assertTrue("The dim blanket should be present in the dom", blanket.isPresent());
        assertTrue("The dim blanket should be visible", blanket.isVisible());
    }

    //Test that dims for dialogs work
    @Test
    public void testDialogDim()
    {
        elementFinder.find(By.id("dialog-button")).click();

        PageElement blanket = elementFinder.find(By.className("aui-blanket"));

        assertTrue("The dim blanket should be present in the dom", blanket.isPresent());
        assertTrue("The dim blanket should be visible", blanket.isVisible());
    }


    //test that the popup shows correctly
    @Test
    public void testPopupShow()
    {
        elementFinder.find(By.id("popup-button")).click();

        PageElement popup = elementFinder.find(By.id("my-popup"));

        assertTrue("The popup should be present", popup.isPresent());
        assertTrue("The popup should be visible", popup.isVisible());
    }

    //test that dialogs show correctly
    @Test
    public void testDialogShow()
    {
        elementFinder.find(By.id("dialog-button")).click();

        PageElement dialog = elementFinder.find(By.id("dialog-test"));

        assertTrue("The popup should be present", dialog.isPresent());
        assertTrue("The popup should be visible", dialog.isVisible());
    }

    //test that popup hides correctly after escape key is hit
    @Test
    public void testPopupEscHide()
    {
        elementFinder.find(By.id("popup-button")).click();

        PageElement popup = elementFinder.find(By.id("my-popup"));

        assertTrue("The popup should be present", popup.isPresent());
        assertTrue("The popup should be visible", popup.isVisible());

        new Actions(product.getTester().getDriver()).sendKeys(Keys.ESCAPE).perform();

        assertFalse("The popup should not be visible", popup.isVisible());
    }

    //test that dialog is hidden correctly after escape key is hit
    @Test
    public void testDialogEscHide()
    {
        elementFinder.find(By.id("dialog-button")).click();

        PageElement dialog = elementFinder.find(By.id("dialog-test"));

        assertTrue("The dialog should be present", dialog.isPresent());
        assertTrue("The dialog should be visible", dialog.isVisible());

        new Actions(product.getTester().getDriver()).sendKeys(Keys.ESCAPE).perform();

        assertFalse("The dialog should not be visible", dialog.isVisible());
    }

    //test that Dialog hides correctly when pressing a close button
    @Test
    public void testDialogButtonHide()
    {
        elementFinder.find(By.id("dialog-button")).click();

        PageElement dialog = elementFinder.find(By.id("dialog-test"));

        assertTrue("The dialog should be present", dialog.isPresent());
        assertTrue("The dialog should be visible", dialog.isVisible());

        dialog.find(By.cssSelector("button.button-panel-button:nth-child(3)")).click();

        assertFalse("The dialog should not be visible", dialog.isVisible());
    }

    //test that popups are set to the correct size
    @Test
    public void testPopupSize()
    {

        elementFinder.find(By.id("popup-button")).click();

        WebDriverElement popup = (WebDriverElement) elementFinder.find(By.id("my-popup"));
        WebElement el = popup.asWebElement();
        Dimension size = el.getSize();

        assertEquals("Dialog: 'my-popup' Height is not 200px", 200, size.getHeight());
        assertEquals("Dialog: 'my-popup' Width is not 400px", 400, size.getWidth());
    }

    //test that dialogs are set to the correct size
    @Test
    public void testDialogSize()
    {
        elementFinder.find(By.id("dialog-button")).click();

        WebDriverElement dialog = (WebDriverElement) elementFinder.find(By.id("dialog-test"));
        WebElement el = dialog.asWebElement();
        Dimension size = el.getSize();

        assertEquals("Dialog: 'dialog-test' Height is not 530px + 2px of border", 532, size.getHeight());
        assertEquals("Dialog: 'dialog-test' Width is not 860px + 2px of border", 862, size.getWidth());
    }

    //test that one stacking dialog works correctly
    @Test
    public void testOneStackingDialogShow()
    {
        elementFinder.find(By.id("dialog-button")).click();

        PageElement dialog = elementFinder.find(By.id("dialog-test"));

        assertTrue("The dialog should be present", dialog.isPresent());
        assertTrue("The dialog should be visible", dialog.isVisible());

        dialog.find(By.cssSelector("button.button-panel-button:nth-child(4)")).click();

        assertTrue("The dialog should still be present", dialog.isPresent());
        assertTrue("The dialog should still be visible", dialog.isVisible());

        PageElement stackedDialog = elementFinder.find(By.id("stack-dialog1"));

        assertTrue("The stacked dialog should be present", stackedDialog.isPresent());
        assertTrue("The stacked dialog should be visible", stackedDialog.isVisible());
    }

    //test that 2 stacking dialogs work correctly
    @Test
    public void testTwoStackingDialogsShow()
    {
        elementFinder.find(By.id("dialog-button")).click();

        PageElement dialog = elementFinder.find(By.id("dialog-test"));

        assertTrue("The dialog should be present", dialog.isPresent());
        assertTrue("The dialog should be visible", dialog.isVisible());

        dialog.find(By.cssSelector("button.button-panel-button:nth-child(4)")).click();

        PageElement stackedDialog = elementFinder.find(By.id("stack-dialog1"));

        stackedDialog.find(By.cssSelector("button.button-panel-button:nth-child(2)")).click();

        PageElement stackedDialog2 = elementFinder.find(By.id("stack-dialog2"));

        assertTrue("The dialog should be present", dialog.isPresent());
        assertTrue("The dialog should be visible", dialog.isVisible());

        assertTrue("The stacked dialog should be present", stackedDialog.isPresent());
        assertTrue("The stacked dialog should be visible", stackedDialog.isVisible());

        assertTrue("The second stacked dialog should be present", stackedDialog2.isPresent());
        assertTrue("The second stacked dialog should be visible", stackedDialog2.isVisible());
    }

    //test addPanel
    @Test
    public void testAddPanel()
    {
        elementFinder.find(By.id("test-add-panel-button")).click();

        PageElement dialog = elementFinder.find(By.id("test-panel-dialog"));

        assertTrue("The dialog should be present", dialog.isPresent());
        assertTrue("The dialog should be visible", dialog.isVisible());
        assertTrue("The dialog panel should be present", dialog.find(By.className("dialog-panel-body")).isPresent());
    }


    //test addButton
    @Test
    public void testAddButton()
    {
        elementFinder.find(By.id("test-add-button-button")).click();

        PageElement dialog = elementFinder.find(By.id("test-button-dialog"));

        assertTrue("The dialog should be present", dialog.isPresent());
        assertTrue("The dialog should be visible", dialog.isVisible());
        assertTrue("The dialog panel should be present", dialog.find(By.className("dialog-button-panel")).isPresent());
        assertTrue("The dialog panel button should be present", dialog.find(By.cssSelector("button.button-panel-button:nth-child(1)")).isPresent());
    }

}
