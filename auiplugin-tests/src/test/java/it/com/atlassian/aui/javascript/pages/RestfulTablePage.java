package it.com.atlassian.aui.javascript.pages;


import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.aui.component.restfultable.RestfulTable;

import javax.inject.Inject;

public class RestfulTablePage implements Page
{
    @Inject
    private PageBinder binder;

    public String getUrl()
    {
        return "/plugins/servlet/ajstest/restfultable/";
    }

    public RestfulTable getTable()
    {
        return binder.bind(RestfulTable.class, "contacts-table");
    }
}
