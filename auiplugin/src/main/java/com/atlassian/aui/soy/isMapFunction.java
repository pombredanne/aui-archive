package com.atlassian.aui.soy;

import com.atlassian.soy.renderer.JsExpression;
import com.atlassian.soy.renderer.SoyClientFunction;
import com.atlassian.soy.renderer.SoyServerFunction;
import com.google.common.collect.ImmutableSet;

import java.util.Map;
import java.util.Set;

public class isMapFunction implements SoyServerFunction<Boolean>, SoyClientFunction {

    public String getName() {
        return "isMap";
    }

    public JsExpression generate(JsExpression... jsExpressions) {
        return new JsExpression("Object.prototype.toString.call(" + jsExpressions[0].getText() + ") === '[object Object]'");
    }

    public Boolean apply(Object... objects) {
        return objects[0] instanceof Map;
    }

    public Set<Integer> validArgSizes() {
        return ImmutableSet.of(1);
    }
}
