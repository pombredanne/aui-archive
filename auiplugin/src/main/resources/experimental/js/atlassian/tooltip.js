(function ($) {
    'use strict';

    $.fn.tooltip = function (options) {
        return this.tipsy($.extend({}, $.fn.tooltip.defaults, options))
    };

    $.fn.tooltip.defaults = {
        opacity: 1.0,
        hoverable: true
    };
}(jQuery));
