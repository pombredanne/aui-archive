Qunit.require('js/external/jquery/jquery.js');
Qunit.require('js/atlassian/atlassian.js');
Qunit.require('js/atlassian/forms.js');

module("Forms Unit Tests", {
    setup: function() {
        AJS.inlineHelp();
    }
});

test("Test inlineHelp", function() {
    var fieldHelpToggle = AJS.$('#fieldHelpToggle');
    var fieldHelp = AJS.$('#field-help');

    ok(fieldHelp.hasClass('hidden'), "The fieldHelp element should have the hidden class");
    fieldHelpToggle.click();
    ok(!fieldHelp.hasClass('hidden'), "The fieldHelp element should not have the hidden class");
    fieldHelpToggle.click();
    ok(fieldHelp.hasClass('hidden'), "The fieldHelp element should have the hidden class again.");
});